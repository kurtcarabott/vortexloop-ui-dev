using SyncfusionBlazorAppSample.Data;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Syncfusion.Blazor;
using System.Globalization;
using Microsoft.JSInterop;
using SyncfusionBlazorAppSample.Shared;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SyncfusionBlazorAppSample
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            // Add your Syncfusion license key for Blazor platform with corresponding Syncfusion NuGet version referred in project. For more information about license key see https://help.syncfusion.com/common/essential-studio/licensing/license-key.
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MzczNjI2QDMxMzgyZTM0MmUzMGd3LzZuYmgzd0lmWmlMM09SbHpzWkp5alArRVVoanM4Lzd6OHNYQ096M2s9;MzczNjI3QDMxMzgyZTM0MmUzMFI0VTAxMWRiMVNQd2NWVTV2OGhtNEQ4VzdWNFZ4Vm9WYXMxWkovZkp2SXc9;MzczNjI4QDMxMzgyZTM0MmUzME1OYzRPL1RUZXpERFBXUngzd3FGblFsT2ZCSlRFNjV1WGR2RWtJeUl3azA9;MzczNjI5QDMxMzgyZTM0MmUzMERpQWJhdTNyTGxEaStySTgxQnB1N1o5Z3JYUm9LejkycUovQ01USUtGSTA9;MzczNjMwQDMxMzgyZTM0MmUzMFpMNXlTb09rbWdxWVhkYVhiNHpUT0p3TDMxbVJXcHFodFM3T0pFdUtXYWc9;MzczNjMxQDMxMzgyZTM0MmUzMFBKbEZXMUJmRWIrM2U3eWI0ZmVWdVZ6TWNkMkFzTmJuOTJwSDdjeW4ya289;MzczNjMyQDMxMzgyZTM0MmUzMEVqSSsvVis4RDB1S3VIRVJWK1k1OGhLbjB6K2loZTFySDZsNUxwUjc1eUE9;MzczNjMzQDMxMzgyZTM0MmUzMFZ5cUFyZFZGalkrVHRIUEJzTks2dWFTKy84U1hWR1NqcEZLaXkwdkNxbUU9;MzczNjM0QDMxMzgyZTM0MmUzMFhFUUVjYUp1VE02OXpjNjZscGVZTWJVWHJEekZQNGQ4RXRBb2ZhSVg0Q0E9;MzczNjM1QDMxMzgyZTM0MmUzMGMxWlF3Y3hVNjVRU1pQaTEyZ29VR09oR3FldmdyL1hiTUtmVVc5dEZJMWs9");
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");
            builder.Services.AddSyncfusionBlazor();
            builder.Services.AddSingleton(typeof(ISyncfusionStringLocalizer), typeof(SyncfusionLocalizer));

            // Set the default culture of the application
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US");
            CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en-US");

                        // Get the modified culture from culture switcher
                        var host = builder.Build();
                        var jsInterop = host.Services.GetRequiredService<IJSRuntime>();
                        var result = await jsInterop.InvokeAsync<string>("cultureInfo.get");
                        if (result != null)
                        {
                            // Set the culture from culture switcher
                            var culture = new CultureInfo(result);
                            CultureInfo.DefaultThreadCurrentCulture = culture;
                            CultureInfo.DefaultThreadCurrentUICulture = culture;
                        }
            builder.Services.AddSingleton<PdfService>();

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            await builder.Build().RunAsync();
        }
    }
}
