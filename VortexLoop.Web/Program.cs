using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Syncfusion.Blazor;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace VortexLoop.Web
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            // SYNCFUSION: Licence (https://help.syncfusion.com/common/essential-studio/licensing/license-key?_ga=2.169395426.210983654.1609085762-718615131.1609085762#how-to-generate-syncfusion-license-key)
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MzczNjI2QDMxMzgyZTM0MmUzMGd3LzZuYmgzd0lmWmlMM09SbHpzWkp5alArRVVoanM4Lzd6OHNYQ096M2s9;MzczNjI3QDMxMzgyZTM0MmUzMFI0VTAxMWRiMVNQd2NWVTV2OGhtNEQ4VzdWNFZ4Vm9WYXMxWkovZkp2SXc9;MzczNjI4QDMxMzgyZTM0MmUzME1OYzRPL1RUZXpERFBXUngzd3FGblFsT2ZCSlRFNjV1WGR2RWtJeUl3azA9;MzczNjI5QDMxMzgyZTM0MmUzMERpQWJhdTNyTGxEaStySTgxQnB1N1o5Z3JYUm9LejkycUovQ01USUtGSTA9;MzczNjMwQDMxMzgyZTM0MmUzMFpMNXlTb09rbWdxWVhkYVhiNHpUT0p3TDMxbVJXcHFodFM3T0pFdUtXYWc9;MzczNjMxQDMxMzgyZTM0MmUzMFBKbEZXMUJmRWIrM2U3eWI0ZmVWdVZ6TWNkMkFzTmJuOTJwSDdjeW4ya289;MzczNjMyQDMxMzgyZTM0MmUzMEVqSSsvVis4RDB1S3VIRVJWK1k1OGhLbjB6K2loZTFySDZsNUxwUjc1eUE9;MzczNjMzQDMxMzgyZTM0MmUzMFZ5cUFyZFZGalkrVHRIUEJzTks2dWFTKy84U1hWR1NqcEZLaXkwdkNxbUU9;MzczNjM0QDMxMzgyZTM0MmUzMFhFUUVjYUp1VE02OXpjNjZscGVZTWJVWHJEekZQNGQ4RXRBb2ZhSVg0Q0E9;MzczNjM1QDMxMzgyZTM0MmUzMGMxWlF3Y3hVNjVRU1pQaTEyZ29VR09oR3FldmdyL1hiTUtmVVc5dEZJMWs9");

            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            builder.Services.AddOidcAuthentication(options =>
            {
                // Configure your authentication provider options here.
                // For more information, see https://aka.ms/blazor-standalone-auth
                builder.Configuration.Bind("Local", options.ProviderOptions);
                options.ProviderOptions.ResponseType = "code";
                options.ProviderOptions.DefaultScopes.Add("profile");
            });

            // Guide: https://blazor.syncfusion.com/documentation/getting-started/blazor-webassembly/                                    
            builder.Services.AddSyncfusionBlazor();
            await builder.Build().RunAsync();
        }
    }
}

