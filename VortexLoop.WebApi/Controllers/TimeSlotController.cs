using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace VortexLoop.WebApi.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class TimeSlotController : ControllerBase
    {
        private readonly ILogger<TimeSlotController> _logger;

        public TimeSlotController(ILogger<TimeSlotController> logger)
        {
            _logger = logger;
        }

        // GET: api/timeslot
        [HttpGet]
        public ActionResult<IEnumerable<TimeSlot>> Get()
        {
            return VortexLoopMockService.GetInstance().TimeSlotLists;    
        }

        // GET: api/timeslot/5
        [HttpGet("{id}", Name = "Get")]
        public ActionResult<TimeSlot> Get(string id)
        {
            TimeSlot ts = VortexLoopMockService.GetInstance().TimeSlotLists
                .Where(x => x.id == id).FirstOrDefault();
            if(ts== null)            
                return NotFound( new { Message = "TimeSlot has not been found."} );
            else
                return Ok(ts);
                       
        }

        // POST: api/timeslot
        [HttpPost]
        public ActionResult<IEnumerable<TimeSlot>> Post(TimeSlot timeSlot)
        {
            VortexLoopMockService.GetInstance().TimeSlotLists
                .Add(new TimeSlot()
                {
                    id = timeSlot.id,
                    createdOn = Convert.ToDateTime(timeSlot.createdOn),
                    createdByUserId = timeSlot.createdByUserId,
                    createdByUsername = timeSlot.createdByUsername,
                    type = timeSlot.type,
                    state = (timeSlot.state == (int)TimeSlot.TimeSlotState.Off) ? TimeSlot.TimeSlotState.Off : TimeSlot.TimeSlotState.On,
                    title = timeSlot.title,
                    description = timeSlot.description,
                    timeFrom = Convert.ToDateTime(timeSlot.timeFrom),
                    timeTo = Convert.ToDateTime(timeSlot.timeTo),
                    relayGroupId = timeSlot.relayGroupId
                });

            return VortexLoopMockService.GetInstance().TimeSlotLists;
        }

        // PUT: api/timeslot/5
        [HttpPut("{id}")]
        public ActionResult<TimeSlot> Put([FromRoute] string id, TimeSlot timeSlot)
        {
            var tS = VortexLoopMockService.GetInstance().TimeSlotLists
                .Where(x => x.id == id).FirstOrDefault();
            var index = VortexLoopMockService.GetInstance().TimeSlotLists.IndexOf(tS);


            tS.createdOn = Convert.ToDateTime(timeSlot.createdOn);
            tS.createdByUserId = timeSlot.createdByUserId;
            tS.createdByUsername = timeSlot.createdByUsername;
            tS.type = timeSlot.type;
            tS.state = (timeSlot.state == (int)TimeSlot.TimeSlotState.Off) ? TimeSlot.TimeSlotState.Off : TimeSlot.TimeSlotState.On;
            tS.title = timeSlot.title;
            tS.description = timeSlot.description;
            tS.timeFrom = Convert.ToDateTime(timeSlot.timeFrom);
            tS.timeTo = Convert.ToDateTime(timeSlot.timeTo);
            tS.relayGroupId = timeSlot.relayGroupId;            

            if (index != -1)
                VortexLoopMockService.GetInstance().TimeSlotLists[index] = tS;

            return VortexLoopMockService.GetInstance().TimeSlotLists[index];
        }

        // DELETE: api/timeslot/5
        [HttpDelete("{id}")]
        public ActionResult<IEnumerable<TimeSlot>> Delete(string id)
        {
            var timeSlot = VortexLoopMockService.GetInstance().TimeSlotLists
                .Where(x => x.id == id).FirstOrDefault();
            VortexLoopMockService.GetInstance().TimeSlotLists.Remove(timeSlot);

            return VortexLoopMockService.GetInstance().TimeSlotLists;
        }
    }


}
