using System;
using System.Collections.Generic;

namespace VortexLoop.WebApi
{
    public class VortexLoopMockService
    {
        // Singlton Schema
        public List<TimeSlot> TimeSlotLists { get; set; }

        // Singlton Setup
        private static readonly VortexLoopMockService _VLMockServiceInstance = new VortexLoopMockService();
        private VortexLoopMockService()
        {
            // On Startup
            PopulateTimeSlots();
        }

        // Singlton Instance
        public static VortexLoopMockService GetInstance() => _VLMockServiceInstance;
        
        // Singlton Methods
        private void PopulateTimeSlots()
        {
            // Check if TimeSlotLists is initated
            if (TimeSlotLists == null)
                TimeSlotLists = new List<TimeSlot>();

            // MOCK DATA PLAN - 3 DAYS - (XmasEve - 24th / XmasDay - 25th / BoxingDay - 26th)

            TimeSlot ts20201224s01 = new TimeSlot()
            {
                id = "2020-0123",
                createdOn = new DateTime(2020, 12, 20, 09, 33, 59),
                createdByUserId = "12341234",
                createdByUsername = "Kurt Carabott",
                type = TimeSlotType.tstExternal.Code,
                state = TimeSlot.TimeSlotState.On,
                title = "Title Text 24 - 01",
                description = "Description longer Text 24 - 01",
                timeFrom = new DateTime(2020, 12, 24, 13, 00, 00),
                timeTo = new DateTime(2020, 12, 24, 14, 30, 00),
                relayGroupId = RelayGroup.rg01.Id
            };
            TimeSlotLists.Add(ts20201224s01);

            TimeSlot ts20201224s02 = new TimeSlot()
            {
                id = "2020-0124",
                createdOn = new DateTime(2020, 12, 20, 09, 37, 03),
                createdByUserId = "12342345",
                createdByUsername = "Joeseph D.",
                type = TimeSlotType.tstExternal.Code,
                state = TimeSlot.TimeSlotState.On,
                title = "Title Text 24 - 02",
                description = "Description longer Text 24 - 02",
                timeFrom = new DateTime(2020, 12, 24, 14, 30, 00),
                timeTo = new DateTime(2020, 12, 24, 16, 00, 00),
                relayGroupId = RelayGroup.rg01.Id
            };
            TimeSlotLists.Add(ts20201224s02);

            TimeSlot ts20201224s03 = new TimeSlot()
            {
                id = "2020-0125",
                createdOn = new DateTime(2020, 12, 20, 09, 33, 59),
                createdByUserId = "12341234",
                createdByUsername = "Kurt Carabott",
                type = TimeSlotType.tstExternal.Code,
                state = TimeSlot.TimeSlotState.On,
                title = "Title Text 24 - 03",
                description = "Description longer Text 24 - 03",
                timeFrom = new DateTime(2020, 12, 24, 17, 30, 00),
                timeTo = new DateTime(2020, 12, 24, 19, 00, 00),
                relayGroupId = RelayGroup.rg01.Id
            };
            TimeSlotLists.Add(ts20201224s03);
        }


    }
}
