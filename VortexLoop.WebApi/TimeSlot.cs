using System;

namespace VortexLoop.WebApi
{
    public class TimeSlot
    {
        public enum TimeSlotState
        { 
            Off = 0,
            On = 1
        }

        // Time Slot Object Info
        public string id { get; set; }                  // External = "12345" | Internal = "{GUID}"

        public DateTime createdOn { get; set; }         // "2018-02-10T09:33:59Z"

        public string createdByUserId { get; set; }     // "12341234"
            
        public string createdByUsername { get; set; }   // "Kurt Carabott"

        public string type { get; set; }                // Booking System = "External" | Committe Schedule = "Internal"  

        // Time Slot Context Info
        public TimeSlotState state { get; set; }        // ON = 1 | OFF = 0

        public string title { get; set; }               // "Title Text"

        public string description { get; set; }         // "Descriprtion text can be longer"

        public DateTime timeFrom { get; set; }          // "2018-02-10T13:00Z"

        public DateTime timeTo { get; set; }            // "2018-02-10T14:30Z"

        public string relayGroupId { get; set; }        // Crt01 = "1" | Crt02 = "2" | Crt03 = "3" | Crt04 = "4"  

    }

    public class RelayGroup
    {
        private RelayGroup(string id) { Id = id; }

        public string Id { get; set; }

        public static RelayGroup rg01 { get { return new RelayGroup("01"); } }
        public static RelayGroup rg02 { get { return new RelayGroup("02"); } }
        public static RelayGroup rg03 { get { return new RelayGroup("03"); } }
        public static RelayGroup rg04 { get { return new RelayGroup("04"); } }        
    }

    public class TimeSlotType
    {
        private TimeSlotType(string code) { Code = code; }

        public string Code { get; set; }

        public static TimeSlotType tstInternal { get { return new TimeSlotType("Internal"); } }
        public static TimeSlotType tstExternal { get { return new TimeSlotType("External"); } }               
    }
}
